import { Component, OnInit, Input } from '@angular/core';
import { ChatserverService } from '../chatserver.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  @Input() message: string;
  userType: string;
  user:string;
  sendData: Object;
  result : any;
  usersList: any;

  constructor(private ChatserverService: ChatserverService) { }

  sendMessage() {
    // console.log(this.userType)
    // console.log(this.changeUser)
    // console.log(this.message)
    // console.log(this)
   this.sendData = {
     user: this.userType,
     message: this.message
   }
    this.ChatserverService.add(this.sendData)
  }

  deleteMes(userType) {
    this.ChatserverService.deleteMessages(userType);
  }
  
  changeUser(user){ // для изменения пользователя 
    console.log(user)
  }

  ngOnInit() {
    this.ChatserverService.getMessages().subscribe(
      result => this.result = result
    );
    this.ChatserverService.getUsers().subscribe(
      usersList => this.usersList = usersList
    );
  }

}
